export default {
  bind(el, binding, vnode) {
    el.clickOutside = (event) => {
      // Clicked outside of the element?
      if (!(el === event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutside);
  },
  unbind(el) {
    document.body.removeEventListener('click', el.clickOutside);
  },
};
