// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAnalytics from 'vue-analytics';
import firebase from 'firebase';

import routes from '@/router/routes';
import store from '@/store';
import App from '@/App';

/* <Setup App's router> */
Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  mode: 'history',
  /* mode: 'hash', */
});

// Check before each page load whether the page requires authentication/
// if it does check whether the user is signed into the web app or
// redirect to the sign-in page to enable them to sign-in
router.beforeEach((to, from, next) => {
  const { currentUser } = firebase.auth();
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const requiresPortfolioTitle = to.matched.some(record => record.meta.requiresPortfolioTitle);

  // TODO: Proper routing
  if (requiresAuth && !currentUser) {
    if (to.name === 'ProfileUserSettings') {
      const userId = to.params.id;

      next({ name: 'ProfileUserInfo', params: { id: userId } });
    } else {
      next({ name: 'AppHome' }); // Go home
    }
  } else if (requiresAuth && currentUser) {
    if (to.name === 'ProfileUserSettings') {
      // If user wants to go to settings of different user
      const userId = to.params.id;

      if (userId !== currentUser.uid) {
        next({ name: 'ProfileUserInfo', params: { id: userId } });
      } else {
        next();
      }
    } else {
      next();
    }
  } else {
    next();
  }

  if (requiresPortfolioTitle) {
    const isPortfolioTitleSet = from.matched.some(record => record.meta.isPortfolioTitleSet);
    if (!isPortfolioTitleSet) {
      next({ name: 'PortfolioNewAnonymous' });
    }
  }
  next();

  /* if (requiresPortfolioType) {
    const setPortfolioType = to.matched.some(record => record.meta.setPortfolioType);
    if (setPortfolioType) {
      next();
    } else {
      next({ name: 'PortfolioNewName' });
    }
  } else {
    next();
  } */

  // next();
});
/* </Setup App's router> */

/* <Setup Vue Analytics> */
Vue.use(VueAnalytics, {
  id: 'UA-113189030-1',
  router,
});
/* </Setup Vue Analytics> */

Vue.config.productionTip = false;

/* <FILTERS> */
Vue.filter('parseNumber', (value) => {
  let output = Number(value.toFixed(2)).toLocaleString('en');

  // Convert billions to B
  if (value >= 1.0e+9) {
    output = `${(value / 1.0e+9).toFixed(3)}B`;
    return output;
  }

  // Convert millions to M
  if (value >= 1.0e+6) {
    output = `${(value / 1.0e+6).toFixed(3)}M`;
    return output;
  }

  return output;
});
Vue.filter('parseNumberMobile', (value) => {
  let output = Number(value.toFixed(2)).toLocaleString('en');

  // Convert billions to B
  if (value >= 1.0e+9) {
    output = `${(value / 1.0e+9).toFixed(3)}B`;
    return output;
  }

  // Convert millions to M
  if (value >= 1.0e+6) {
    output = `${(value / 1.0e+6).toFixed(3)}M`;
    return output;
  }

  // Convert thousands to K
  if (value >= 1.0e+3) {
    output = `${(value / 1.0e+3).toFixed(1)}K`;
    return output;
  }

  return output;
});
Vue.filter('parsePercent', value => `${Number(value).toFixed(2)}%`);
Vue.filter('timeSince', (value) => {
  const seconds = Math.floor((new Date() - value) / 1000);

  let interval = Math.floor(seconds / 31536000);
  if (interval > 1) {
    return `${interval} years`;
  }

  interval = Math.floor(seconds / 2592000);
  if (interval > 1) {
    return `${interval} months`;
  }

  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return `${interval} days`;
  }

  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return `${interval} hours`;
  }

  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return `${interval} minutes`;
  }

  interval = Math.floor(seconds);
  return `${interval} seconds`;
});
/* </FILTERS> */

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App },
});
