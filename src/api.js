export const PriceBase = 'https://min-api.cryptocompare.com/data/price';
export const PriceHistoricalBase = 'https://min-api.cryptocompare.com/data/pricehistorical';
export const PriceMultiFullBase = 'https://min-api.cryptocompare.com/data/pricemultifull';
