import AppHome from '@/components/AppHome';
import AppPrivacyPolicy from '@/components/AppPrivacyPolicy';

/* import LandingPage from '@/components/LandingPage'; */

import ProfileUser from '@/components/ProfileUser';
import ProfileUserInfo from '@/components/ProfileUserInfo';
import ProfileUserPortfolios from '@/components/ProfileUserPortfolios';
import ProfileUserSettings from '@/components/ProfileUserSettings';

import PortfolioPage from '@/components/PortfolioPage';

import PortfolioNew from '@/components/PortfolioNew';
import PortfolioNewAnonymous from '@/components/PortfolioNewAnonymous';
/* import PortfolioNewType from '@/components/PortfolioNewType'; */
import PortfolioNewName from '@/components/PortfolioNewName';
import PortfolioNewList from '@/components/PortfolioNewList';

export default
[
  {
    path: '/',
    name: 'AppHome',
    component: AppHome,
  },
  /* {
    path: '/home',
    name: 'AppHome',
    component: AppHome,
  },
  {
    path: '/',
    name: 'LandingPage',
    component: LandingPage,
  }, */
  {
    path: '/user/:id',
    component: ProfileUser,
    children: [
      {
        path: '',
        redirect: { name: 'ProfileUserInfo' },
      },
      {
        path: 'info',
        name: 'ProfileUserInfo',
        component: ProfileUserInfo,
      },
      {
        path: 'portfolios',
        name: 'ProfileUserPortfolios',
        component: ProfileUserPortfolios,
      },
      {
        path: 'settings',
        name: 'ProfileUserSettings',
        component: ProfileUserSettings,
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
  {
    path: '/portfolio/:id',
    name: 'PortfolioPage',
    component: PortfolioPage,
  },
  {
    path: '/post/new',
    component: PortfolioNew,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: '',
        name: 'PortfolioNewAnonymous',
        component: PortfolioNewAnonymous,
      },
      /* {
        path: 'type',
        name: 'PortfolioNewType',
        component: PortfolioNewType,
      }, */
      {
        path: 'name',
        name: 'PortfolioNewName',
        component: PortfolioNewName,
        meta: {
          isPortfolioTitleSet: false,
        },
      },
      {
        path: 'coins',
        name: 'PortfolioNewList',
        component: PortfolioNewList,
        meta: {
          requiresPortfolioTitle: true,
        },
      },
    ],
  },
  {
    path: '/info/policy',
    name: 'AppPrivacyPolicy',
    component: AppPrivacyPolicy,
  },
  {
    path: '/info/tos',
    name: 'AppTos',
    component: AppPrivacyPolicy,
  },
];
