import firebase from 'firebase';

export default {
  methods: {
    async postComment(userId, username, photoUrl, portfolioId, commentString) {
      const trimmed = commentString.trim();

      const batch = firebase.firestore().batch();

      const userCommentRef = firebase.firestore().collection(`portfolio-comments/users/${userId}`).doc();
      // Use same ID to save comment under portfolios
      const commentId = userCommentRef.id;

      batch.set(userCommentRef, {
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        comment: trimmed,
        username,
        photoUrl,
        portfolioId,
        userId,
        commentId,
      });

      const portfolioCommentRef = firebase.firestore().collection(`portfolio-comments/portfolios/${portfolioId}`).doc(commentId);
      batch.set(portfolioCommentRef, {
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        comment: trimmed,
        username,
        photoUrl,
        portfolioId,
        userId,
        commentId,
      });

      await batch.commit();
      return commentId;
    },

    async getPortfolioComments(portfolioId, isFirstBatch, lastDoc, batchSize) {
      let query = {};
      if (isFirstBatch) {
        query = firebase.firestore().collection(`portfolio-comments/portfolios/${portfolioId}`)
          .orderBy('timestamp', 'desc')
          .limit(batchSize);
      } else {
        query = firebase.firestore().collection(`portfolio-comments/portfolios/${portfolioId}`)
          .orderBy('timestamp', 'desc')
          .startAfter(lastDoc)
          .limit(batchSize);
      }

      return query.get();
    },

    async getPortfolioCommentCount(portfolioId) {
      let totalCount = 0;
      const ref = firebase.firestore().doc(`counters/portfolio-comments/portfolios/${portfolioId}`);
      const snapshot = await ref.collection('shards').get();
      snapshot.forEach((doc) => {
        totalCount += doc.data().count;
      });
      return totalCount;
    },

    // Returns N last portfolio comments by user
    async getLastPortfolioCommentsByUser(userId, count) {
      const query = firebase.firestore()
        .collection(`portfolio-comments/users/${userId}`)
        .orderBy('timestamp', 'desc')
        .limit(count);

      const snapshot = await query.get();
      const comments = [];
      snapshot.forEach((doc) => {
        comments.push(doc.data());
      });

      return comments;
    },

    async deletePortfolioComment(userId, portfolioId, commentId) {
      const batch = firebase.firestore().batch();

      const userCommentRef = firebase.firestore().doc(`portfolio-comments/users/${userId}/${commentId}`);
      const portfolioCommentRef = firebase.firestore().doc(`portfolio-comments/portfolios/${portfolioId}/${commentId}`);

      batch.delete(userCommentRef);
      batch.delete(portfolioCommentRef);

      await batch.commit();
    },
  },
};
