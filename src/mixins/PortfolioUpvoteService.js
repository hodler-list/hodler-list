import firebase from 'firebase';

export default {
  methods: {
    async upvotePortfolio(portfolioId, userId) {
      const batch = firebase.firestore().batch();

      const userUpvoteRef = firebase.firestore().doc(`portfolio-upvotes/users/${userId}/${portfolioId}`);
      batch.set(userUpvoteRef, { timestamp: firebase.firestore.FieldValue.serverTimestamp() });

      const portfolioUpvoteRef = firebase.firestore().doc(`portfolio-upvotes/portfolios/${portfolioId}/${userId}`);
      batch.set(portfolioUpvoteRef, { timestamp: firebase.firestore.FieldValue.serverTimestamp() });

      await batch.commit();
    },

    async cancelPortfolioUpvote(portfolioId, userId) {
      const batch = firebase.firestore().batch();

      const userUpvoteRef = firebase.firestore().doc(`portfolio-upvotes/users/${userId}/${portfolioId}`);
      batch.delete(userUpvoteRef);

      const portfolioUpvoteRef = firebase.firestore().doc(`portfolio-upvotes/portfolios/${portfolioId}/${userId}`);
      batch.delete(portfolioUpvoteRef);

      await batch.commit();
    },

    async getPortfolioUpvoteCount(portfolioId) {
      let totalCount = 0;
      const upvoteCounterRef = firebase.firestore().doc(`counters/portfolio-upvotes/portfolios/${portfolioId}`);
      const snapshot = await upvoteCounterRef.collection('shards').get();
      snapshot.forEach((doc) => {
        totalCount += doc.data().count;
      });
      return totalCount;
    },

    async hasUserUpvotedPortfolio(portfolioId, userId) {
      const ref = firebase.firestore().doc(`portfolio-upvotes/users/${userId}/${portfolioId}`);
      const snapshot = await ref.get();

      if (snapshot.exists) {
        return true;
      }
      return false;
    },
  },
};
