import firebase from 'firebase';

export default {
  methods: {
    async getUsername(userId) {
      const userRef = firebase.firestore().doc(`users/${userId}`);
      const snapshot = await userRef.get();

      if (snapshot.exists) {
        return snapshot.data().username;
      }
      return null;
    },

    async writeUser(userId, username, email, photoUrl) {
      // Write both user object and username atomically

      const userRef = firebase.firestore().doc(`users/${userId}`);
      const userSnapshot = await userRef.get();

      if (!userSnapshot.exists) {
        const batch = firebase.firestore().batch();
        batch.set(userRef, {
          userId,
          username,
          email,
          photoUrl,
        });

        const usernameRef = firebase.firestore().doc(`usernames/${username}`);
        batch.set(usernameRef, { userId });

        await batch.commit();
      }
    },

    async getUser(userId) {
      const ref = firebase.firestore().doc(`users/${userId}`);
      const snapshot = await ref.get();
      if (snapshot.exists) {
        return snapshot.data();
      }
      return null;
    },

    async checkIfUsernameAvailable(username) {
      const edited = username.trim().toLowerCase();
      const ref = firebase.firestore().doc(`usernames/${edited}`);
      const snapshot = await ref.get();
      if (snapshot.exists) {
        return false;
      }
      return true;
    },

    async checkIfUserExists(userId) {
      const ref = firebase.firestore().doc(`users/${userId}`);
      const snapshot = await ref.get();
      if (snapshot.exists) {
        return true;
      }
      return false;
    },

    async updateUserData(userId, email, photoUrl) {
      // Don't change signing email
      // Only email in DB
      const ref = firebase.firestore().doc(`users/${userId}`);
      const snapshot = await ref.get();

      if (snapshot.exists) {
        ref.update({
          email,
          photoUrl,
        });
      }
    },

    async updateUserEmail(userId, email) {
      // Don't change signing email
      // Only email in DB
      const ref = firebase.firestore().doc(`users/${userId}`);
      const snapshot = await ref.get();

      if (snapshot.exists) {
        await ref.update({ email });
      }
    },

    async updateUsername(userId, oldUsername, newUsername) {
      const userRef = firebase.firestore().doc(`users/${userId}`);
      const userSnapshot = await userRef.get();

      if (userSnapshot.exists) {
        // Check if an old username belongs to this user
        const oldUsernameRef = firebase.firestore().doc(`usernames/${oldUsername}`);
        const oldUsernameSnapshot = await oldUsernameRef.get();

        if (oldUsernameSnapshot.exists) {
          const oldUsernameUserId = oldUsernameSnapshot.data().userId;
          if (userId === oldUsernameUserId) {
            const batch = firebase.firestore().batch();
            const newUsernameRef = firebase.firestore().doc(`usernames/${newUsername}`);

            batch.delete(oldUsernameRef);
            batch.update(userRef, { username: newUsername });
            batch.set(newUsernameRef, { userId });
            await batch.commit();
          }
        }
      }
    },
  },
};
