import firebase from 'firebase';

export default {
  methods: {
    async getPortfolioHistoricalValues24h(portfolioId) {
      const query = firebase.firestore().collection(`portfolios/${portfolioId}/historicalValues24h`)
        .orderBy('timestamp', 'asc');

      return query.get();
    },

    async getTopPortfoliosByChange(count) {
      const query = firebase.firestore().collection('portfolios')
        .orderBy('changeSinceInception', 'desc')
        .limit(count);

      const portfolios = [];
      const snapshot = await query.get();

      await Promise.all(snapshot.docs.map(async (doc) => {
        const portfolioId = doc.id;

        const portfolioRef = firebase.firestore().doc(`portfolios/${portfolioId}`);
        const portfolioSnapshot = await portfolioRef.get();
        if (portfolioSnapshot.exists) {
          portfolios.push(portfolioSnapshot.data());
        }
      }));

      return portfolios;
    },

    async getPortfolioSnapshots(isFirstBatch, lastDoc, batchSize) {
      let query = {};
      if (isFirstBatch) {
        query = firebase.firestore().collection('portfolios').orderBy('timestamp', 'desc').limit(batchSize);
      } else {
        query = firebase.firestore().collection('portfolios').orderBy('timestamp', 'desc').startAfter(lastDoc)
          .limit(batchSize);
      }
      return query.get();
    },

    async getUsersPortfolioSnapshots(isFirstBatch, lastDoc, batchSize, userId) {
      let query = {};
      if (isFirstBatch) {
        query = firebase.firestore().collection('portfolios')
          .where('userId', '==', userId)
          .orderBy('timestamp', 'desc')
          .limit(batchSize);
      } else {
        query = firebase.firestore().collection('portfolios')
          .where('userId', '==', userId)
          .orderBy('timestamp', 'desc')
          .startAfter(lastDoc)
          .limit(batchSize);
      }
      return query.get();
    },

    // Returns first N portfolios upvoted by a user
    async getLastPortfoliosUpvotedByUser(userId, count) {
      // Fetch ids of upvoted portfolios first
      const queryForPortfoliosIds = firebase.firestore()
        .collection(`portfolio-upvotes/users/${userId}`)
        .orderBy('timestamp', 'desc')
        .limit(count);

      const snapshot = await queryForPortfoliosIds.get();
      const promises = [];
      snapshot.docs.forEach((doc) => {
        const portfolioId = doc.id;
        const portfolioRef = firebase.firestore().doc(`portfolios/${portfolioId}`);
        promises.push(portfolioRef.get());
      });

      const portfolioSnapshots = await Promise.all(promises);
      const portfolios = [];
      portfolioSnapshots.forEach((s) => {
        if (s.exists) {
          portfolios.push(s.data());
        }
      });

      return portfolios;
    },

    async getPortfolio(portfolioId) {
      const portfolioRef = firebase.firestore().doc(`portfolios/${portfolioId}`);
      const snapshot = await portfolioRef.get();
      if (snapshot.exists) {
        return snapshot.data();
      }
      return null;
    },

    async writePortfolio(portfolio, userId/* , currentPortfolioValue */) {
      const portfolioRef = firebase.firestore().collection('portfolios').doc();
      const portfolioId = portfolioRef.id;

      const portfolioObj = portfolio;
      portfolioObj.id = portfolioId;
      portfolioObj.userId = userId;
      portfolioObj.timestamp = firebase.firestore.FieldValue.serverTimestamp();

      await portfolioRef.set(portfolioObj);
      return portfolioId;
    },
  },
};
