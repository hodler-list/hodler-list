import axios from 'axios';
import { PriceBase, PriceMultiFullBase } from '@/api';

export default {
  methods: {
    async getPricePercentChange24h(coinId) {
      const response = await axios.get(PriceMultiFullBase, {
        params: {
          fsyms: coinId,
          tsyms: 'USD',
        },
      });

      if (response.data.Response === 'Error') {
        return 0;
      }

      const change = parseFloat(response.data.DISPLAY[coinId].USD.CHANGEPCT24HOUR);
      return change;
    },

    async getCurrentPrice(coinId) {
      const response = await axios.get(PriceBase, {
        params: {
          fsym: coinId,
          tsyms: 'USD',
        },
      });

      if (response.data.Response === 'Error') {
        return 0;
      }

      const price = parseFloat(response.data.USD);
      return price;
    },

    // Returns current price and percent price change in 24h
    async getCoinData(coinId) {
      const currentPrice = await this.getCurrentPrice(coinId);
      const percentChange = await this.getPricePercentChange24h(coinId);

      return {
        id: coinId,
        price: currentPrice,
        percentChange24h: percentChange,
      };
    },

    async getCoinGain(coin) {
      const coinId = coin.id;
      const amount = parseFloat(coin.amount);
      const price = parseFloat(await this.getCurrentPrice(coinId));

      const gain = amount * price;
      return {
        id: coinId,
        gain,
      };
    },
  },
};
