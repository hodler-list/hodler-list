import firebase from 'firebase';

export default {
  namespaced: true,
  state: {
    currentUser: null,
    user: null,
  },
  mutations: {
    setCurrentUser(state) {
      state.currentUser = firebase.auth().currentUser;
    },
    unsetCurrentUser(state) {
      state.currentUser = null;
    },
    setUser(state, userData) {
      state.user = userData;
    },
  },
  actions: {
    setCurrentUser({ commit }) {
      commit('setCurrentUser');
    },
    unsetCurrentUser({ commit }) {
      commit('unsetCurrentUser');
    },
    async writeUser({ rootState }, {
      userId, username, displayName, email, photoURL,
    }) {
      const currentUserRef = rootState.db.collection('users').doc(userId);
      const userSnapshot = currentUserRef.get();
      if (!userSnapshot.exists) {
        await rootState.db.collection('users').doc(userId).set({
          userId,
          username,
          displayName,
          email,
          photoURL,
          createdOn: firebase.firestore.FieldValue.serverTimestamp(),
        });
      }
    },
    async setUser({ commit, rootState, state }, userId) {
      state.user = null; // Rather display nothing than old user data
      const userRef = rootState.db.collection('users').doc(userId);
      const userSnapshot = await userRef.get();
      if (userSnapshot.exists) {
        commit('setUser', userSnapshot.data());
      } else {
        throw new Error('User doesn\'t exist');
      }
    },
    async updateUserData({ rootState }, {
      userId, displayName, username, email, photoURL,
    }) {
      const userRef = rootState.db.collection('users').doc(userId);
      const userSnapshot = await userRef.get();
      if (userSnapshot.exists) {
        // Update in the DB
        const updateObj = { };
        if (displayName) {
          updateObj.displayName = displayName;
        }
        if (username) {
          updateObj.username = username;
        }
        if (email) {
          updateObj.email = email;
        }
        if (photoURL) {
          updateObj.photoURL = photoURL;
        }
        await rootState.db.collection('users').doc(userId).update(updateObj);
      } else {
        throw new Error('User doesn\'t exist');
      }
    },

    /* async updateCurrentUserData({ rootState }, {
      userId, email, photoURL,
    }) {
      const currentUserRef = rootState.db.collection('users').doc(userId);
      const userSnapshot = await currentUserRef.get();
      if (userSnapshot.exists) {
        await rootState.db.collection('users').doc(userId).update({
          email,
          photoURL,
          lastSignedIn: firebase.firestore.FieldValue.serverTimestamp(),
        });
      }
    },
    async updateUserData({ rootState }, {
      userId, displayName, username, email,
    }) {
      await rootState.db.collection('users').doc(userId).update({
        displayName,
        username,
        email,
      });
    }, */
  },
  getters: {
    getCurrentUser(state) {
      return state.currentUser;
    },
    getUser(state) {
      return state.user;
    },
  },
};
