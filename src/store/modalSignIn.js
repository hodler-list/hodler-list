export default {
  namespaced: true,
  state: {
    isModalSignInActive: false,
    isSigningUp: false,
    showProvidersButtons: false,
    errorMessage: '',
    isLoading: false,
  },
  mutations: {
    setModalSignInActive(state, isActive) {
      state.isModalSignInActive = isActive;
    },
    setIsSigningUp(state, value) {
      state.isSigningUp = value;
    },
    setShowProvidersButtons(state, value) {
      state.showProvidersButtons = value;
    },
    setErroMessage(state, value) {
      state.errorMessage = value;
    },
    setIsLoading(state, value) {
      state.isLoading = value;
    },
  },
  actions: {
    setModalSignInActive({ commit }, isActive) {
      commit('setModalSignInActive', isActive);
    },
    setIsSigningUp({ commit }, value) {
      commit('setIsSigningUp', value);
    },
    setShowProvidersButtons({ commit }, value) {
      commit('setShowProvidersButtons', value);
    },
    setErrorMessage({ commit }, value) {
      commit('setErroMessage', value);
    },
    setIsLoading({ commit }, value) {
      commit('setIsLoading', value);
    },
    resetState({ commit }) {
      commit('setIsSigningUp', false);
      commit('setShowProvidersButtons', false);
      commit('setErroMessage', '');
      commit('setIsLoading', false);
    },
  },
  getters: {
    getIsModalSignInActive(state) {
      return state.isModalSignInActive;
    },
    getIsSigningUp(state) {
      return state.isSigningUp;
    },
    getShowProvidersButtons(state) {
      return state.showProvidersButtons;
    },
    getErrorMessage(state) {
      return state.errorMessage;
    },
    getIsLoading(state) {
      return state.isLoading;
    },
  },
};
