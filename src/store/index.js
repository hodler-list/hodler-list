import Vue from 'vue';
import Vuex from 'vuex';

import firebase from 'firebase';
import 'firebase/firestore';

import user from '@/store/user';
import search from '@/store/search';
import portfolioNew from '@/store/portfolioNew';
import portfolioPage from '@/store/portfolioPage';
import modalSignIn from '@/store/modalSignIn';
import topPortfolios from '@/store/topPortfolios';

/* <Setup Firebase> */
const config = {
  apiKey: 'AIzaSyDQpZr8C2Y01ybzB1Y7dJ-Hz7Cn2wDirhw',
  authDomain: 'hodler-list.firebaseapp.com',
  databaseURL: 'https://hodler-list.firebaseio.com',
  projectId: 'hodler-list',
  storageBucket: 'hodler-list.appspot.com',
  messagingSenderId: '309043267558',
};
firebase.initializeApp(config);
/* </Setup Firebase> */

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    db: firebase.firestore(),
  },
  modules: {
    user,
    search,
    portfolioNew,
    portfolioPage,
    modalSignIn,
    topPortfolios,
  },
});
