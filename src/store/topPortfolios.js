export default {
  namespaced: true,
  state: {
    topByUpvotes: [],
    topByChange: [],
  },
  mutations: {
    setTopByUpvotes(state, portfolios) {
      state.topByUpvotes = [...portfolios];
    },
    setTopByChange(state, portfolios) {
      state.topByChange = [...portfolios];
    },
  },
  actions: {
    setTopByUpvotes({ commit }, portfolios) {
      commit('setTopByUpvotes', portfolios);
    },
    setTopByChange({ commit }, portfolios) {
      commit('setTopByChange', portfolios);
    },
  },
  getters: {
    getTopByUpvotes(state) {
      return state.topByUpvotes;
    },
    getTopByChange(state) {
      return state.topByChange;
    },
  },
};
