export default {
  namespaced: true,
  state: {
    isAnonymous: false,
    portfolioType: 0,
    portfolioName: '',
    portfolioNote: '',
    coins: [],
    coinInputIndexes: [0],
    portfolioValue: 1,
  },
  mutations: {
    setPortfolioAnonymous(state, isAnonymous) {
      state.isAnonymous = isAnonymous;
    },
    setPortfolioType(state, type) {
      state.portfolioType = type;
    },
    setPortfolioName(state, name) {
      state.portfolioName = name;
    },
    setPortfolioNote(state, note) {
      state.portfolioNote = note;
    },
    addCoin(state, coin) {
      state.coins.push(coin);

      state.portfolioValue = 0;
      state.coins.forEach((c) => {
        state.portfolioValue += parseFloat(c.amount * c.buyPrice);
      });
    },
    removeCoin(state, removeKey) {
      state.coins = state.coins.filter(({ key }) => key !== removeKey);

      state.portfolioValue = 0;
      state.coins.forEach((c) => {
        state.portfolioValue += parseFloat(c.amount * c.buyPrice);
      });
    },
    addNewCoinInputIndex(state) {
      const lastIndex = state.coinInputIndexes.slice(-1).pop();
      state.coinInputIndexes.push(lastIndex + 1);
    },
    removeCoinInputIndex(state, key) {
      state.coinInputIndexes =
        state.coinInputIndexes.filter(coinInputIndex => coinInputIndex !== key);
    },
    updateCoin(state, { key, updatedCoin }) {
      const coinIndex = state.coins.findIndex(coin => coin.key === key);
      if (coinIndex !== -1) {
        state.coins[coinIndex] = updatedCoin;

        state.portfolioValue = 0;
        state.coins.forEach((c) => {
          state.portfolioValue += parseFloat(c.amount * c.buyPrice);
        });
      }
    },
    resetState(state) {
      state.isAnonymous = false;
      state.portfolioType = 0;
      state.portfolioName = '';
      state.portfolioNote = '';
      state.coins = [];
      state.coinInputIndexes = [0];
      state.portfolioValue = 0;
    },
  },
  actions: {
    setPortfolioAnonymous({ commit }, isAnonymous) {
      commit('setPortfolioAnonymous', isAnonymous);
    },
    setPortfolioType({ commit }, type) {
      commit('setPortfolioType', type);
    },
    setPortfolioName({ commit }, name) {
      commit('setPortfolioName', name);
    },
    setPortfolioNote({ commit }, note) {
      commit('setPortfolioNote', note);
    },
    addCoin({ commit }, coin) {
      commit('addCoin', coin);
    },
    removeCoin({ commit }, removeKey) {
      commit('removeCoin', removeKey);
    },
    addNewCoinInputIndex({ commit }) {
      commit('addNewCoinInputIndex');
    },
    removeCoinInputIndex({ commit }, key) {
      commit('removeCoinInputIndex', key);
    },
    updateCoin({ commit }, { key, updatedCoin }) {
      commit('updateCoin', { key, updatedCoin });
    },
    resetState({ commit }) {
      commit('resetState');
    },
  },
  getters: {
    getPortfolioAnonymous(state) {
      return state.isAnonymous;
    },
    getPortfolioType(state) {
      return state.portfolioType;
    },
    getPortfolioName(state) {
      return state.portfolioName;
    },
    getPortfolioNote(state) {
      return state.portfolioNote;
    },
    getCoins(state) {
      return state.coins;
    },
    getCoinInputIndexes(state) {
      return state.coinInputIndexes;
    },
    getPortfolioValue(state) {
      return state.portfolioValue;
    },
    getCreatedPortfolio(state) {
      const portfolio = {
        isAnonymous: state.isAnonymous,
        type: state.portfolioType,
        name: state.portfolioName,
        note: state.portfolioNote,
        coins: state.coins,
        inceptionValue: state.portfolioValue,
      };

      return portfolio;
    },
  },
};
