export default {
  namespaced: true,
  state: {
    portfolio: null,
    coinsGains: null,
    coinsChartColors: null,
    portfolioValue: null,
    portfolioValuePercentChange24h: null,
  },
  mutations: {
    setPortfolio(state, portfolio) {
      state.portfolio = portfolio;
    },
    setCoinsGains(state, gains) {
      state.coinsGains = gains;
    },
    setCoinsChartColors(state, colors) {
      state.coinsChartColors = colors;
    },
    setPortfolioValue(state, value) {
      state.portfolioValue = value;
    },
    setPortfolioValuePercentChange24h(state, change) {
      state.portfolioValuePercentChange24h = change;
    },
  },
  actions: {
    setPortfolio({ commit }, portfolio) {
      commit('setPortfolio', portfolio);
    },
    setCoinsGains({ commit }, gains) {
      commit('setCoinsGains', gains);
    },
    setCoinsChartColors({ commit }, colors) {
      commit('setCoinsChartColors', colors);
    },
    setPortfolioValue({ commit }, value) {
      commit('setPortfolioValue', value);
    },
    setPortfolioValuePercentChange24h({ commit }, change) {
      commit('setPortfolioValuePercentChange24h', change);
    },
  },
  getters: {
    getPortfolio(state) {
      return state.portfolio;
    },
    getCoinsGains(state) {
      return state.coinsGains;
    },
    getCoinsChartColors(state) {
      return state.coinsChartColors;
    },
    getPortfolioValue(state) {
      return state.portfolioValue;
    },
    setPortfolioValuePercentChange24h(state) {
      return state.portfolioValuePercentChange24h;
    },
  },
};
