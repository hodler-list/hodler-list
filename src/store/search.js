import algoliaserach from 'algoliasearch';

/* <Setup Algolia> */
const client = algoliaserach('ZK4RP54K90', '2352c7d122dbe9296339f228f39d88c2');
/* </Setup Algolia> */

export default {
  namespaced: true,
  state: {
    indexCoins: client.initIndex('coins'),
  },
  getters: {
    getIndexCoins(state) {
      return state.indexCoins;
    },
  },
};
